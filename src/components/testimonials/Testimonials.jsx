import React from 'react';
import { Element } from 'react-scroll';
import './testimonials.css';

function Testimonials(props) {
    var testimonials = props.data.testimonials.map(function(testimonials){
        return  <li key={testimonials.user}>
            <blockquote>
               <p>{testimonials.text}</p>
               <cite>{testimonials.user}</cite>
            </blockquote>
         </li>
    })

    return(
        <Element name="testimonials" className="testimonials">
            <div className="container">
                <div className="row">
                    <div className="col-md-2 pt-2">
                        <h1><span>Client Testimonials</span></h1>
                    </div>
                    <div className="col-md-10">
                        <ul className="slides">
                            {testimonials}
                        </ul>
                    </div>
                </div>
            </div>
        </Element>
    );
}

export default Testimonials;