import React from 'react';
import { Element } from 'react-scroll';
import './contact.css';

function Contact() {
    return(
        <Element name="contact" className="contact">
            <h1>This is Contact section</h1>
        </Element> 
    );

}

export default Contact;